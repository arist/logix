# How to use the ECG

Here's some basic info on how to use the ECG functionality of your card10

## Finger ECG

![Top View Harmony Board](top_1-4.jpg "Finger ECG and Pulse Oxymeter")

Your card10 comes with a number of fun things to play with

  - 1   : Contact,  negative ECG electrode 
  - 2   : Pluse oxymeter (can [potentially, if at some point supported by software] measure pulse and blood oxygen levels using an optical sensor)
  - 3   : Contact,  negative ECG electrode (again)
  - 4   : Screw (also works as a negative electrode, see [8])


![Bottom View - ](bottom_5-6.jpg "COM for Bias and positive electrode for finger ECG")

  - 5 : COM (for bias)
  - 6 : Contact, positive ECG electrode 

![Side View Details](side_front_7-8.jpg "Pin and isolation")

  - 7 : Pin for soldering/stitching an alternative electrode, if you don't want to use (6)
  - 8 : Plastic distancer, works as an isolation (see [4])

## USB ECG

  - see [ECG Kit Assembly](../ecg_kit_assembly/) on how to solder your ECG cable
  -  NB: it DOES matter how you connect the kit via USB. if you get a lot of noise, try to turn the connection


Have fun experimenting with the ECG.
