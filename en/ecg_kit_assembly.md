---
title: ECG kit assembly
---

### How-To wire and solder an ECG-Cable for the Card10 badge

Stick some insulating tape over the ECG contacts at the bottom of your wristband, since we are replacing this contact area with one of the electrodes.

Cut off the USB-A connector as we don't have use for it

[![cut usb wire](/media/ecg/1-cut-wire.jpg)](/media/ecg/1-cut-wire.jpg)

Slide the big shrinking tube over the USB cable

Separate the wires as shown in the picture

[![usb wire meanings](/media/ecg/wire-meanings.jpg)](/media/ecg/wire-meanings.jpg)


Cut off all wires ecxept for orange and purple

[![cut off unused wires](/media/ecg/cut-off-wires.jpg)](/media/ecg/cut-off-wires.jpg)


Move the two small shrinking tubes over the ECG wires

Solder the ECG wires to the USB wires as shown in the pictures

[![solder ecg usb wires](/media/ecg/3-solder.jpg)](/media/ecg/3-solder.jpg)



Test your ECG setup on your Card10 prior the next step

Slide the small shrinking tubes over the soldering connections and apply hot air (e.g. lighter)

[![apply small heat shrink tubes](/media/ecg/4-add-heat-shrink.jpg)](/media/ecg/4-add-heat-shrink.jpg)

[![shrink small heat shrink tubes](/media/ecg/5-shrik-this.jpg)](/media/ecg/5-shrik-this.jpg)



Slide the bigger shrinking tube over the two small ones and also shrink it

[![apply large heat shrink tube](/media/ecg/large-heat-shrink.jpg)](/media/ecg/large-heat-shrink.jpg)


Have fun and hack ECG stuff

[![ecg fertig](/media/ecg/fertig.jpg)](/media/ecg/fertig.jpg)



### Usage

[![ecg usage](/media/ecg/application.jpg)](/media/ecg/application.jpg)


#### ECG
Place the electrodes on your chest as shown in the picture and connect the USB-C connector with the USB symbol downwards. After starting the ECG application on your Card10 press the right lower button (RIGHT) to switch over to ECG over USB.

If you want to start logging your ECG data press the lower left button (LEFT) to start and stop logging. Log files are written to the filesystem (ecg-\<time\>.log) and can be transferred to a computer using the boot loader. You can visualize them with Audacity: Use File→Import→Raw Data, select the log file, then use Signed 16-bit PCM, Little Endian, 1 Channel, offset 0, amount 100% and sample rate 128 Hz. Normalize the amplitude by selecting the entire track (Ctrl-A), then use Effect→Amplify and keep the suggested Amplification value.

#### Eye movement detection and muscle activity
To detect eye movements like fast movements to the left or right or to detect REM phase while at sleep, place your electrodes on your temples.
